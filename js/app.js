angular.module('app', []).directive('dnd', function () {
    return {
        link: function ($scope, elem, attr) {
            elem.bind('dragover', function (e) {
                e.stopPropagation();
                e.preventDefault();
                // jQuery wraps native event object,
                // but we can still access it via 'originalEvent' property
                e.originalEvent.dataTransfer.dropEffect = 'copy';
                $(this).addClass('over');
            });
            elem.bind('dragleave', function (e) {
                $(this).removeClass('over');
            });
            elem.bind('drop', function (e) {
                e.stopPropagation();
                e.preventDefault();
                $(this).removeClass('over');
                $scope.handleDrop(e);
            });
        }
    }
});

function Translator($scope) {
    const MIME_TYPE = 'text/plain';
    var jsonData = {
        from: {},
        to: {}
    };

    $scope.items = [];
    $scope.orderProp = 'id';
    $scope.onlyEmpty = false;
    $scope.fromText = 'Drop original JSON here';
    $scope.toText = 'Drop translation JSON here';

    $scope.handleDrop = function (e) {
        var dt = e.originalEvent.dataTransfer,
            isSingleJson = dt.files.length === 1 && dt.files[0].name.substr(-4) === 'json';
        if (!isSingleJson) return;
        var file = dt.files[0],
            prop = e.target.dataset.prop;
        $scope.$apply(function () {
            $scope[prop + 'Text'] = file.name;
        });
        getJsonData(file, prop);
    };

    $scope.generateDownload = function () {
        var data = {},
            i, item, text, blob, a;
        for (i = 0; i < $scope.items.length; i++) {
            item = $scope.items[i];
            if (item.to !== '') {
                data[item.id] = item.to;
            }
        }
        text = JSON.stringify(data, null, 4); // pretty-print JSON
        blob = new Blob([text], {type: MIME_TYPE});
        a = document.createElement('a');
        a.href = window.URL.createObjectURL(blob);
        a.download = $scope.toText;
        a.dataset.downloadurl = [MIME_TYPE, a.download, a.href].join(':');
        a.textContent = 'Download ready';
        a.draggable = true;
        a.onclick = function () {
            if ('disabled' in this.dataset) {
                return false;
            }
            clearLink(this);
        };
        a.ondragstart = function (e) {
            e.dataTransfer.setData('DownloadURL', e.target.dataset.downloadurl);
        };
        a.ondragend = function (e) {
            clearLink(e.target);
        };
        $('#download').html('').show().append(a);
    };

    function clearLink(a) {
        a.textContent = 'Downloaded';
        a.dataset.disabled = true;
        // Need a small delay for the revokeObjectURL to work properly.
        setTimeout(function() {
            window.URL.revokeObjectURL(a.href);
        }, 1500);
    }

    function getJsonData(file, prop) {
        loadTextFile(file, onLoad);
        function onLoad(text) {
            try {
                var data = JSON.parse(text);
                jsonData[prop] = data;
            } catch (e) {
                alert('Invalid JSON.');
            }
            if (Object.keys(jsonData.from).length && Object.keys(jsonData.to).length) {
                rebuildItems();
            }
        }
    }

    function rebuildItems() {
        var items = [],
            id, to;
        for (id in jsonData.from) {
            to = jsonData.to[id];
            items.push({
                id: id,
                from: jsonData.from[id],
                to: typeof to === 'string' ? to : ''
            });
        }
        $scope.$apply(function () {
            $scope.items = items;
        });
    }

    function loadTextFile(file, callback) {
        var reader = new FileReader();
        // if we use onloadend, we need to check the readyState
        reader.onloadend = function (event) {
            if (event.target.readyState === FileReader.DONE) { // DONE == 2
                typeof callback === 'function' && callback(event.target.result);
            }
        }
        var blob = file.slice(0, file.size);
        reader.readAsText(blob);
    }
}